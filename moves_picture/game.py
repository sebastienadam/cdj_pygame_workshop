#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

# variables
BG_COLOR  = pygame.Color(255,255,255) # background color
MV_DOWN   = K_DOWN                    # Key to move down
MV_LEFT   = K_LEFT                    # Key to move left
MV_RIGHT  = K_RIGHT                   # Key to move right
MV_UP     = K_UP                      # Key to move up
MV_WIDTH  = 8                         # Movement size
FPS       = 10                        # game speed
PICTURE   = "data/apple.png"          # picture to show
SIZE      = 640,480                   # game size
SND_STEP  = "data/step_01.wav"        # Sound to be played in after each step
SND_ERROR = "data/error_01.wav"       # Sound to be played in case of error
START_POS = 256,176                   # starting position

# initialize game
pygame.init()
screen = pygame.display.set_mode(SIZE)
pygame.display.set_caption('Moving image')
pict = pygame.image.load(PICTURE)
pictrect = pict.get_rect()
pictrect.move_ip(START_POS)
clock = pygame.time.Clock()

## Check if it's possible to play sound
if pygame.mixer and not pygame.mixer.get_init():
    print ('Warning, no sound')
    pygame.mixer = None

# main game loop
while True:
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()
    keystate = pygame.key.get_pressed()
    if any(keystate):
        # A key has been pressed, calculate the next position
        x = pictrect.x
        y = pictrect.y
        if keystate[MV_DOWN]:
            y = pictrect.y + MV_WIDTH
        if keystate[MV_LEFT]:
            x = pictrect.x - MV_WIDTH
        if keystate[MV_RIGHT]:
            x = pictrect.x + MV_WIDTH
        if keystate[MV_UP]:
            y = pictrect.y - MV_WIDTH
        if y < 0 or y + pictrect.h > SIZE[1] or x < 0 or x + pictrect.w > SIZE[0]:
            # The picture go outside the screen. Don't move ans play error sound
            if pygame.mixer:
                pygame.mixer.music.load(SND_ERROR)
                pygame.mixer.music.play(1)
        elif pictrect.x != x or pictrect.y != y:
            # Move the picture and play step sound
            pictrect.x = x
            pictrect.y = y
            if pygame.mixer:
                pygame.mixer.music.load(SND_STEP)
                pygame.mixer.music.play(1)
    screen.fill(BG_COLOR)
    screen.blit(pict, pictrect)
    pygame.display.update()
    clock.tick(FPS)
