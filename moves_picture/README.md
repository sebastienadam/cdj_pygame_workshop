# PyGame exercises

## Move picture

![screenshot](screenshot.png)

In this example, you will put a picture on the screen and move it with keyboard. Make sure it doesn't come off the screen. At each step, a sound is played. If the image tries to exit the screen, play an error sound.

By modifying parameters, you can change the picture or the played sounds. You can also change the keys used to move the picture.

### Related techniques

* Putting image on the screen
* Moving object
* Keyboard management
* Playing sound

### Possible enhancements

* Add some obstacles
* Add more picture that can be moved
* ...
