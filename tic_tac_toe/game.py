#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *
from TicTacToe import TicTacToe
from config import *

# initialize game
pygame.init()
screen = pygame.display.set_mode(SIZE)
pygame.display.set_caption('Tic tac toe')
clock = pygame.time.Clock()
grid = TicTacToe(screen)

# main game loop
while True:
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()
        if event.type == MOUSEBUTTONDOWN and event.button == 1:
            grid.click(event.pos)
    grid.draw()
    pygame.display.update()
    clock.tick(FPS)
