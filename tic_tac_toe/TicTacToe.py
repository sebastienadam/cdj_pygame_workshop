#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame, sys
from config import *

class TicTacToe():
    def __init__(self, screen):
        self._screen = screen
        screen_size = screen.get_size()
        self._grid_sep = int(min([i / 4 for i in screen_size]))
        self._cell_padding = int(self._grid_sep / 20)
        self._grid_width = self._grid_sep * 3
        self._grid_start_x = int((screen_size[0] - self._grid_width) / 2)
        self._grid_start_y = int((screen_size[1] - self._grid_width) / 2)
        if pygame.mixer and not pygame.mixer.get_init():
            print ('Warning, no sound')
            self._mixer = None
        else:
            self._mixer = pygame.mixer
        self._color_cross = PLAYR1_COLOR
        self._color_circle = PLAYR2_COLOR
        self._grid = None
        self._turn = None
        self._ended = None
        self._winner = None
        self._reset_game()

    def _a_property_ended(self):
        """ Indicates whether a game is finished or not. """
        return self._ended
    ended = property(_a_property_ended)

    def _a_property_winner(self):
        """ Give me the winning player. 0 indicates that there is no player. None indicate that the game is not finished. """
        return self._winner
    winner = property(_a_property_winner)

    def _draw_circle(self, pos_x, pos_y):
        """ draw a circle (O) into the grid """
        radius = int((self._grid_sep - 2 * self._cell_padding) / 2)
        center_x = self._grid_start_x + self._grid_sep * pos_x + radius + self._cell_padding
        center_y = self._grid_start_y + self._grid_sep * pos_y + radius + self._cell_padding
        pygame.draw.circle(self._screen, self._color_circle, (center_x, center_y), radius, 4)

    def _draw_cross(self, pos_x, pos_y):
        """ draw a cross (X) into the grid """
        from_x = self._grid_start_x + self._grid_sep * pos_x + self._cell_padding
        from_y = self._grid_start_y + self._grid_sep * pos_y + self._cell_padding
        to_x = from_x + self._grid_sep - 2 * self._cell_padding
        to_y = from_y + self._grid_sep - 2 * self._cell_padding
        pygame.draw.line(self._screen, self._color_cross, (from_x, from_y), (to_x, to_y), 4)
        pygame.draw.line(self._screen, self._color_cross, (to_x, from_y), (from_x, to_y), 4)

    def _draw_grid(self):
        """ draw the grid (#) """
        for i in range(2):
            from_x = self._grid_start_x
            to_x = from_x + self._grid_sep * 3
            from_y = self._grid_start_y + self._grid_sep * (1 + i)
            to_y = from_y
            pygame.draw.line(self._screen, GRID_COLOR, (from_x, from_y), (to_x, to_y), 2)
        for i in range(2):
            from_x = self._grid_start_x + self._grid_sep * (1 + i)
            to_x = from_x
            from_y = self._grid_start_y
            to_y = from_y + self._grid_sep * 3
            pygame.draw.line(self._screen, GRID_COLOR, (from_x, from_y), (to_x, to_y), 2)

    def _draw_pos(self):
        """ draw the grid content """
        for x in range(len(self._grid)):
            for y in range(len(self._grid[x])):
                if self._grid[x][y] == 1:
                    self._draw_cross(x, y)
                elif self._grid[x][y] == 2:
                    self._draw_circle(x, y)

    def _draw_winner(self):
        """ Check if there is a winner and draw a line on the winning line """
        self._winner = None
        for x in range(3):
            if all(self._grid[x]) and self._grid[x][0] == self._grid[x][1] and self._grid[x][1] == self._grid[x][2]:
                from_x = self._grid_start_x + self._grid_sep * (1 + x) - self._grid_sep / 2
                to_x = from_x
                from_y = self._grid_start_y
                to_y = from_y + self._grid_sep * 3
                self._winner = self._grid[x][0]
                break
            if all([self._grid[i][x] for i in range(3)]) and self._grid[0][x] == self._grid[1][x] and self._grid[1][x] == self._grid[2][x]:
                from_x = self._grid_start_x
                to_x = from_x + self._grid_sep * 3
                from_y = self._grid_start_y + self._grid_sep * (1 + x) - self._grid_sep / 2
                to_y = from_y
                self._winner = self._grid[0][x]
                break
        if self._winner is None:
            if all([self._grid[i][i] for i in range(3)]) and self._grid[0][0] == self._grid[1][1] and self._grid[1][1] == self._grid[2][2]:
                from_x = self._grid_start_x
                to_x = from_x + self._grid_sep * 3
                from_y = self._grid_start_y
                to_y = from_y + self._grid_sep * 3
                self._winner = self._grid[0][0]
            elif all([self._grid[i][-i-1] for i in range(3)]) and self._grid[0][2] == self._grid[1][1] and self._grid[1][1] == self._grid[2][0]:
                from_x = self._grid_start_x
                to_x = from_x + self._grid_sep * 3
                from_y = self._grid_start_y + self._grid_sep * 3
                to_y = self._grid_start_y
                self._winner = self._grid[0][2]
        if self._winner is not None:
            if not self._ended:
                self._play_sound(SND_WIN)
                self._ended = True
            pygame.draw.line(self._screen, BLUE, (from_x, from_y), (to_x, to_y), 8)
        elif self._turn == 9 and not self._ended:
            self._play_sound(SND_NOWIN)
            self._ended = True
            self._winner = 0

    def _play_sound(self, sound, repeat = 1):
        """ Play sound if mixer is initialized and PLAY_SOUND parameter is True """
        if self._mixer and PLAY_SOUND:
            self._mixer.music.load(sound)
            self._mixer.music.play(repeat)

    def _reset_game(self):
        """" Reinitialize the game """
        self._grid = [[0 for i in range(3)] for j in range(3)]
        self._turn = 0
        self._ended = False
        self._winner = None

    def click(self, pos):
        """ detect click on the screen """
        if self._ended:
            self._reset_game()
        else:
            click_pos_x, click_pos_y = pos
            pos_x, pos_y = None, None
            for i in range(3):
                from_x = self._grid_start_x + self._grid_sep * i
                to_x = self._grid_start_x + self._grid_sep * (i + 1)
                from_y = self._grid_start_y + self._grid_sep * i
                to_y = self._grid_start_y + self._grid_sep * (i + 1)
                if pos_x is None and click_pos_x > from_x and click_pos_x < to_x:
                    pos_x = i
                if pos_y is None and click_pos_y > from_y and click_pos_y < to_y:
                    pos_y = i
            if (pos_x is not None) and (pos_y is not None):
                if self._grid[pos_x][pos_y] == 0:
                    self._grid[pos_x][pos_y] = self._turn % 2 + 1
                    self._turn += 1
                    self._play_sound(SND_STEP)
                else:
                    self._play_sound(SND_ERROR)

    def draw(self):
        """ draw the screen """
        self._screen.fill(BG_COLOR)
        self._draw_grid()
        self._draw_pos()
        self._draw_winner()

