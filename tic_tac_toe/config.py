#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame

# colors
BLACK = pygame.Color(  0,   0,   0)
WHITE = pygame.Color(255, 255, 255)
BLUE  = pygame.Color(  0,   0, 255)
GREEN = pygame.Color(  0, 255,   0)
RED   = pygame.Color(255,   0,   0)

# variables
BG_COLOR     = WHITE                  # background color
FPS          = 10                     # game speed
GRID_COLOR   = BLACK                  # color of the grid
PLAYR1_COLOR = RED                    # color of the player 1
PLAYR2_COLOR = GREEN                  # color of the player 2
PLAY_SOUND   = True                   # If False, disable sound
SIZE         = 640,640                # game size
SND_ERROR    = "data/error_01.wav"    # Sound to be played in case of error
SND_NOWIN    = "data/oh_01.wav"       # Sound to be played when no winner
SND_STEP     = "data/step_01.wav"     # Sound to be played in after each step
SND_WIN      = "data/applause_02.wav" # Sound to be played when winning
WNLN_COLOR   = BLUE                   # Color of the line that indicate the winner
