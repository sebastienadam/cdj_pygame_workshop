# PyGame exercises

## Tic-Tac-Toe

![screenshot](screenshot.png)

> _Tic-tac-toe (also known as noughts and crosses or Xs and Os) is a paper-and-pencil game for two players, X and O, who take turns marking the spaces in a 3x3 grid. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row wins the game._

Source: https://en.wikipedia.org/wiki/Tic-tac-toe

In this example, you will create a game to play Tic-Tac-Toe. Plays different sounds when a user draws his symbols, if he is wrong or when there is a winner.

Here, I have separated the logic of the game from its management. You can use the game as a "black box" in the following way:

````python
from TicTacToe import TicTacToe

#(...)
grid = TicTacToe(screen)
#(...)
grid.click(mouse_position)
#(...)
if grid.ended:
    print('Game ended!')
    print('The winner is: {}'.format(grid.winner))
#(...)
````

The parameters are saved in the file `config.py` and the game itself if coded in the file `game.py`.

### Related techniques

* Drawing shapes (circles, lines)
* Mouse management
* Playing sound
* [Object-oriented programming](https://en.wikipedia.org/wiki/Object-oriented_programming)

### Possible enhancements

* Add score
* ...
