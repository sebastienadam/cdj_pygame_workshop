# PyGame exercises

Here are some exercises for [PyGame](http://www.pygame.org/).

![Logo Pygame](http://www.pygame.org/docs/pygame_logo.gif)
[![Logo CoderDojo Belgium](logo-cdj-be.png)](https://www.coderdojobelgium.be/)

PyGame is a [Python](https://www.python.org/) library allowing you to create multimedia applications (games). These exercises are designed to teach programming to children. You can download the scripts and ask participants to modify some parameters to see what's going on (beginners), add some features (intermediate), or redevelop everything from scratch (advanced).

PyGame reference: http://pygame.org/docs/

## Exercises

Here is a list of the exercises and their related techniques:

1. [Hello Wold!](hello_world/)
    * Showing text
1. [Draw shapes](draw_shapes/)
    * Drawing shapes (rectangles, circle, line)
1. [Draw](draw/)
    * Drawing shapes (lines)
    * Colors
    * Mouse management
1. [Move picture](moves_picture/)
    * Putting image on the screen
    * Moving object
    * Keyboard management
    * Playing sound
1. [Tic-Tac-Toe](tic_tac_toe/)
    * Drawing shapes (circles, lines)
    * Mouse management
    * Playing sound
    * [Object-oriented programming](https://en.wikipedia.org/wiki/Object-oriented_programming)

## Credits

The sounds were downloaded from https://freesound.org/.

The pictures were downloaded from https://pixabay.com/.
