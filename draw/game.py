#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

# initialize game
BG_COLOR  = pygame.Color(255,255,255) # background color
B1_COLOR  = pygame.Color(255,  0,  0) # color for left click
B2_COLOR  = pygame.Color(  0,255,  0) # color for center click
B3_COLOR  = pygame.Color(  0,  0,255) # color for right click
FPS       = 40                        # game speed
SIZE      = 640,480                   # game size

pygame.init()
screen = pygame.display.set_mode(SIZE)
screen.fill(BG_COLOR)
pygame.display.set_caption('Draw')
clock = pygame.time.Clock()
prev_pos = None

# main game loop
while True:
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()
    mousestate = pygame.mouse.get_pressed()
    if any(mousestate):
        if mousestate[0]:     # left button
            color = B1_COLOR
        elif mousestate[1]:   # center button
            color = B2_COLOR
        else:                 # right button
            color = B3_COLOR
        current_pos = pygame.mouse.get_pos()
        # screen.set_at(current_pos, color)
        # pygame.draw.circle(screen, color, current_pos, 4)
        if prev_pos is not None:
            pygame.draw.line(screen, color, prev_pos, current_pos, 4)
        prev_pos = current_pos
    else:
        prev_pos = None
    pygame.display.update()
    clock.tick(FPS)
