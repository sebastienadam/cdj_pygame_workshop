# PyGame exercises

## Draw

![screenshot](screenshot.png)

In this exercise, you will use the mouse to draw on the screen. You have different colors for left button, center button and right button.

### Related techniques

* Drawing shapes (lines)
* Colors
* Mouse management

### Possible enhancements

* Color picker
* Eraser
* ...
