#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

# initialize game
pygame.init()
screen = pygame.display.set_mode((400, 100))
pygame.display.set_caption('Hello World!')
font = pygame.font.SysFont('Arial',60)
text = font.render('Hello World!', True, pygame.Color(0,0,0))
textrect = text.get_rect()

# main loop
while True: # main game loop
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    screen.fill(pygame.Color(255,255,255))
    screen.blit(text, textrect)
    pygame.display.update()
