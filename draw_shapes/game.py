#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

# colors
BLACK = pygame.Color(  0,   0,   0)
WHITE = pygame.Color(255, 255, 255)
BLUE  = pygame.Color(  0,   0, 255)
GREEN = pygame.Color(  0, 255,   0)
RED   = pygame.Color(255,   0,   0)

# initialize game
FPS       = 40                        # game speed
SIZE      = 640,480                   # game size
pygame.init()
screen = pygame.display.set_mode(SIZE)
screen.fill(WHITE)
pygame.display.set_caption('Draw shapes')
clock = pygame.time.Clock()

# draw rectangle
from_x = 32
from_y = 32
width = 256
height = 128
rect = pygame.Rect(from_x, from_y, width, height)
pygame.draw.rect(screen, RED, rect)      # filled rectangle
pygame.draw.rect(screen, BLACK, rect, 4) # rectangle borders

# draw circle
center_x = 512
center_y = 352
radius = 96
pygame.draw.circle(screen, BLUE, (center_x, center_y), radius)     # filled circle
pygame.draw.circle(screen, BLACK, (center_x, center_y), radius, 4) # circle borders

# draw circle
from_x = 608
from_y = 32
to_x = 32
to_y = 464
pygame.draw.line(screen, GREEN, (from_x,from_y), (to_x,to_y), 8)

pygame.display.update()

# main game loop
while True:
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()
    clock.tick(FPS)
