# PyGame exercises

## Draw shapes

![screenshot](screenshot.png)

In this example, you will draw some shapes on the screen.

### Related techniques

* Drawing shapes (rectangles, circle, line)
